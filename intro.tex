Multiword expressions (MWEs) such as \ile{\lex{red tape}}, \ile{\lex{by and large}}, \ile{to \lex{make} a \lex{decision}} and \ile{to \lex{break} one's \lex{heart}} are combinations of words exhibiting unexpected lexical, morphological, syntactic, semantic, pragmatic and/or statistical behavior \cite{baldwin-kim:2010:handbook}. Most prominently, they are semantically non-compositional, that is, their meaning cannot be deduced from the meanings of their components and from their syntactic structure in a way deemed regular. For this reason, the presence of MWEs in texts calls for dedicated treatment, whose prerequisites include their automatic identification. 

The goal of \textit{MWE identification} is, given some input running text, to identify all MWEs' lexicalized components present in it \cite{Schneider-etAl-2016-dimsum,Savary2017}.\footnote{A \textit{lexicalized component} of a MWE, or \textit{component} for short -- marked in bold in examples -- is one which is always realized by the same lexeme. For instance, the noun \ile{decision} is lexicalized in \ile{to \lex{make} a \lex{decision}} but the determiner \ile{a} is not, because it can be replaced by other determiners 
%variants of this MWE can omit it 
(e.g.~\ile{to \lex{make} this/any/the \lex{decision}}).} 
%\todo[inline]{They should add a definition of a variant (which is related to a previous occurrence in the corpus). Thus they will not have to precise all over the paper at each time "of previously seen VWME".}
Such systems face three main challenges: variability, ambiguity and discontinuity \cite{Constantetal17}. Variability refers to the fact that the components of the same MWE can occur in different surface forms because of morphological and/or syntactic transformations, as in example (\ref{ex:en:break-heart-idio}) vs.~(\ref{ex:en:break-heart-pass}) below. Variability is closely related to the issue of unseen data in machine learning -- items whose surface forms differ between test and training data  are usually harder to identify than those seen in identical forms \cite{Augenstein:2017:GNE:3067935.3068030}. MWE ambiguity, conversely, makes the identification of seen MWEs harder, because a particular combination of words can be a MWE in some contexts, as in (\ref{ex:en:break-heart-idio}) and (\ref{ex:en:break-heart-seen}), but not necessarily all, because of \textit{literal readings} (\ref{ex:en:break-heart-lit}) and \textit{coincidental co-occurrence} of the MWE components (\ref{ex:en:break-heart-coin}) \cite{SavaryCordeiro18}.\footnote{Literal readings and coincidental occurrences of MWEs are maked by wavy and dashed underlining, respectively.} %In the final version cite the TLT 2018 paper.
Finally, external elements can occur in between MWEs' components, both seen and unseen (\ref{ex:en:break-heart-idio})--(\ref{ex:en:break-heart-pass}). Discontinuity is known to be a challenge notably for methods based on sequence labeling.

\begin{examples}
\vspace{-3mm}\item \label{ex:en:break-heart-idio} \ile{He \lex{broke} the \lex{heart} of his class mate.}
\vspace{-3mm}\item \label{ex:en:break-heart-seen} \ile{He \lex{broke} her young \lex{heart} when he \lex{let} her \lex{down}.}
\vspace{-3mm}\item \label{ex:en:break-heart-pass} \ile{Just think of all the \lex{hearts broken} by him.}
\vspace{-3mm}\item \label{ex:en:break-heart-lit} \ile{He \litr{broke} her chocolate \litr{heart} when he stepped on it.}
\vspace{-3mm}
\item \label{ex:en:break-heart-coin} \ile{When her toy \cco{broke}, her young \cco{heart} was in sorrow.}
%\vspace{-3mm}\item \label{ex:en:pull-leg-idio} \ile{The man was \lex{pulling} my \lex{leg} but I didn't believe him.}
%\vspace{-3mm}\item \label{ex:en:pull-leg-lit} \ile{\# The kid was \litr{pulling} my \litr{legs}.}
%\vspace{-3mm}\item \label{ex:en:pull-leg-pass} \ile{\# My\litr{leg} was \litr{pulled}.}
%\vspace{-3mm}\item \label{ex:en:pull-leg-coin} \ile{The kid was \flitr{puling} a toy with \flitr{her leg}}
\vspace{-3mm}
\end{examples}


%\begin{examples}
%\vspace{-3mm}\item \label{ex:en:pull-leg-idio} \ile{The man was \lex{pulling} my \lex{leg} but I didn't believe him}
%\vspace{-3mm}\item \label{ex:en:pull-leg-lit} \ile{The kid was \litr{pulling} my \litr{leg} to make me play with him}
%\vspace{-3mm}\item \label{ex:en:pull-leg-coin} \ile{As a result of \cco{pulling} my \cco{leg} became numb}
%\vspace{-3mm}\item \label{ex:en:pull-leg-disc} \ile{Go \lex{pull} someone else's \lex{leg}}.
%\vspace{-3mm}\item \label{ex:en:pull-leg-coin} \ile{The kid was \flitr{puling} a toy with \flitr{her leg}}
%\vspace{-3mm}
%\end{examples}

In this paper we address the three aforementioned challenges at a time by considering a sub-problem of MWE identification, namely \textit{MWE variant identification}, %(MWEVI)\todo{CR: I suggest not introduce a non-standard acronym here because there will be many useful acronyms later in the paper}, 
that is, the identification of occurrences of  known (seen) MWEs and their linguistic variants. For instance, suppose that we have seen the annotated MWE occurrence in (\ref{ex:en:break-heart-idio}) in some training data. We would like to correctly predict that the same MWE occurs also in (\ref{ex:en:break-heart-seen}) and (\ref{ex:en:break-heart-pass}) but not in (\ref{ex:en:break-heart-lit}) and (\ref{ex:en:break-heart-coin}). Notice that we do not aim at identifying the MWE \ile{\lex{let down}} in example (\ref{ex:en:break-heart-seen}) if it has not been previously seen. This focus on MWE variants has important theoretical and practical motivations discussed in Sec.~\ref{sec:related}.

MWEs are known to have specific variability profiles, e.g.~the idiom in (\ref{ex:en:break-heart-idio})--(\ref{ex:en:break-heart-pass}) admits passivization and noun inflection, while others do not, e.g.~\exidio{he was \lex{pulling} my \lex{leg}}{he was kidding me} vs.~\ile{he was \litr{pulling} my \litr{legs}}; \ile{my \litr{leg} was \litr{pulled}}. 
Our research question is whether this MWE-specific variability profile may be captured automatically and leveraged for high-quality MWE variant identification. The idea is that defining a variability profile in terms of linguistically informed variation-oriented features should help, on the one hand, to bring different variants of the same MWE together and, on the other hand, to distinguish idiomatic occurrences of a MWE from its literal readings.  We are specifically interested in morphological and syntactic variants of verbal MWEs (VMWEs). This phenomenon is particularly challenging in morphologically rich languages, notably due to the internal inflection of the VMWE components and to the relatively free word order. In this paper we are interested in VMWEs in French, which exhibits rich verbal inflection and moderate inflection of nouns and adjectives. 

This paper is organized as follows. We discuss the state of the art in MWE variant modeling and identification (Sec.~\ref{sec:related}). We address aspects of VMWE variability in French (Sec.~\ref{sec:variability}), and we describe the corpus used for our experiments (Sec.~\ref{sec:corpus}). Follows a presentation of our a baseline variant identification method (Sec.~\ref{sec:baseline}).
We then propose linguistic features for classification and their extraction process, as well as the architecture of our VMWE variant identification system (Sec.~\ref{sec:indent}). Finally, we present and analyze our results (Sec.~\ref{sec:results}), before we conclude and propose perspectives for future work (Sec.~\ref{sec:conc}).

%In previous ML-based MWEI approaches, unseen data are usually understood as the data with surface forms present only in the test but not in the training data \cite{Augenstein:2017:GNE:3067935.3068030}.
%We model the seen vs. unseen data problem in a more linguistically-informed way, based on the type-token distinction. Namely, we consider that a MWE token $E1$ contained in the test corpus has been already \textit{seen}, if there is another token $E2$ in the train corpus so that $E1$ and $E2$ belong to the same MWE type. This 

%Use the "jouer rôle" example





%Research question: Can variability of MWEs be modeled and measured in a linguistically motivated, cross-linguistically valid and computationally useful way? 

%Method: use linguistically motivated variability-related features in the task of the recognition of MWE variants. Evaluate the increase in performances wrt. a baseline.

%Terminology:\\
%For a given annotated VMWE $E$, we distinguish its: (i) variants $V(E)$, (ii) non-variants $NV(E)$. The latter includes: (a) literal readings of $E$ (\ile{tourner le dos au public} vs. \ile{le président tourne le dos à la légalité internationale}), (b) accidental co-occurrence of $E$'s lexicalized components,  (c) occurrences of $E$'s lexicalized components in other VMWEs (\ile{\lex{se poser} des \lex{questions}} vs. \ile{\lex{poser} une \lex{question}}).

